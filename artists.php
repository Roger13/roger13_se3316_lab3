<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="us-ansi">
        <!--<meta http-equiv="Content-Type" content="text/html;charset=ISO-8859-1">-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <title>Lab3 SE3316A</title>
        
        <link href='https://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Cuprum' rel='stylesheet' type='text/css'>
        <link href="files/css/css.css" rel="stylesheet" type="text/css">      
    
        <!-- Bootstrap core CSS -->
        <link href="files/css/bootstrap.css" rel="stylesheet">
    
        <!-- Custom styles for this template -->
        <link href="files/css/lab3.css" rel="stylesheet">
        <!--<link href="carousel.css" rel="stylesheet">-->
    
    
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="../../assets/js/html5shiv.js"></script>
          <script src="../../assets/js/respond.min.js"></script>
        <![endif]-->
  </head>
  <body>
      
          <header>
        
           <div id="topHeaderRow">
              <div class="container">
                 <nav class="navbar navbar-inverse " role="navigation">
                    <div class="navbar-header">
                       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                       </button>
                       <p class="navbar-text">Welcome to <strong>Art Store</strong>, <a href="#" class="navbar-link">Login</a> or <a href="#" class="navbar-link">Create new account</a></p>
                    </div>
        
                    <div class="collapse navbar-collapse navbar-ex1-collapse pull-right">
                       <ul class="nav navbar-nav">
                          <li><a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
                          <li><a href="#"><span class="glyphicon glyphicon-gift"></span> Wish List</a></li>
                          <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</a></li>
                          <li><a href="#"><span class="glyphicon glyphicon-arrow-right"></span> Checkout</a></li>                  
                       </ul>
                    </div>  <!-- end .collpase --> 
                 </nav>  <!-- end .navbar --> 
              </div>  <!-- end .container --> 
           </div>  <!-- end #topHeaderRow --> 
           
           <div id="logoRow">
              <div class="container">
                 <div class="row">
                    <div class="col-md-8">
                        <h1>Art Store</h1> 
                    </div>
                    
                    <div class="col-md-4">
                       <form class="form-inline" role="search">
                          <div class="input-group">
                             <label class="sr-only" for="search">Search</label>
                             <input class="form-control" placeholder="Search" name="search" type="text">
                             <span class="input-group-btn">
                             <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                             </span>
                          </div>
                       </form> 
                    </div>   <!-- end .navbar --> 
                 </div>   <!-- end .row -->        
              </div>  <!-- end .container --> 
           </div>  <!-- end #logoRow --> 
           
           <div id="mainNavigationRow">
              <div class="container">
        
                 <nav class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                       </button>
                    </div>
        
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                     <ul class="nav navbar-nav">
                       <li><a href="index.php">Home</a></li>
                       <li><a href="about.php">About Us</a></li>
                       <li><a href="work.php">Art Works</a></li>
                       <li class="active"><a href="#">Artists</a></li>
                       <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown">Specials <b class="caret"></b></a>
                         <ul class="dropdown-menu">
                           <li><a href="#">Special 1</a></li>
                           <li><a href="#">Special 2</a></li>                   
                         </ul>
                       </li>
                     </ul>              
                    </div>
                 </nav>  <!-- end .navbar --> 
              </div>  <!-- end container -->
           </div>  <!-- end mainNavigationRow -->
           
        </header>
        
        <div class="container">
        <h2>This Week's Best Artists</h2>
           <div class="alert alert-warning" role="alert">Each week we show you who are our best artists ...</div>   
           <div class="row">
               <div id="myCarousel" class="carousel slide" data-ride="carousel">
                   <div class="carousel-inner">
                               <?php
                                    $artists = fopen("artists.txt", "r");
                                    $counter = -1;
        
                                    while ($counter < 21) {
                                        if ($counter == -1) {
                                            echo "<div class='item active'>";
                                            $counter = 0;
                                        } else {
                                            echo "<div class='item'>";
                                        }
                                        
                                        echo "<div class='container'>";
                                        
                                        while((!feof($artists)) && ($counter%7 != 6)) {
                                            $counter += 1;
                                            $line = explode("~", fgets($artists));
                                            
                                            echo "<div class='col-md-2'>
            											<div class='thumbnail'>
            												<img src='files/portraits/". $line[0] .".jpg' style='width:175px; height:175px;'><br>
            												<div class='caption'>
            													<h4>". $line[1] ." ". $line[2] ."</h4>
            													<p><a class='btn btn-info' role='button' href='". $line[7] ."'>Learn more</a></p>
            												</div>
            											</div>
            										</div>";
                                         
                                        }
                                        
                                        $counter += 1;
                                        
                                        echo "</div>
                                            </div>";
                                            
                                    }
                               ?>
                            </div>


                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
              <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
           
               </div>
              </div>
            <!--</div>-->
           <!--</div>-->
           
           <div class="row">
               <div id="myCarousel2" class="carousel slide" data-ride="carousel">
                   <div class="carousel-inner">
                       <?php
                                    while (!feof($artists)) {
                                        if ($counter <= 22) {
                                            echo "<div class='item active'>";
                                        } else {
                                            echo "<div class='item'>";
                                        }
                                        
                                        echo "<div class='container'>";
                                        
                                        while((!feof($artists)) && ($counter%7 != 6)) {
                                            $counter += 1;
                                            $line = explode("~", fgets($artists));
                                            
                                            echo "<div class='col-md-2'>
            											<div class='thumbnail'>
            												<img src='files/portraits/". $line[0] .".jpg' style='width:175px; height:175px;'><br>
            												<div class='caption'>
            													<h4>". $line[1] ." ". $line[2] ."</h4>
            													<p><a class='btn btn-info' role='button' href='". $line[7] ."'>Learn more</a></p>
            												</div>
            											</div>
            										</div>";
                                         
                                        }
                                        
                                        $counter += 1;
                                        
                                        echo "</div>
                                            </div>";
                                            
                                    }
                               ?>
                       </div>
                <a class="left carousel-control" href="#myCarousel2" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
              <a class="right carousel-control" href="#myCarousel2" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
           
               </div>
              </div>
           <h4>Artists by Genre</h4>
           <div class="progress">
             <div class="progress-bar progress-bar-info" style="width: 7%">
               <span>Gothic</span>
             </div>
             <div class="progress-bar progress-bar-success" style="width: 27%">
               <span>Renaissance</span>
             </div>
             <div class="progress-bar progress-bar-warning" style="width: 15%">
               <span>Baroque</span>
             </div>
             <div class="progress-bar progress-bar-danger" style="width: 21%">
               <span>Pre-Modern</span>
             </div>  
             <div class="progress-bar" style="width: 30%">
               <span>Modern</span>
             </div>
           </div>
        </div>
        
        <!-- Bootstrap core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="files/js/jquery.js"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src="files/js/bootstrap.js"></script>
    </body>
</html>