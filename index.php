<!DOCTYPE html>
<html lang="en">
  <head>
     
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
      
      
    <title>Lab3 SE3316A</title>
    
    <link href='https://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Cuprum' rel='stylesheet' type='text/css'>
    <link href="files/css/css.css" rel="stylesheet" type="text/css">  

    <!-- Bootstrap core CSS -->
    <link href="files/css/bootstrap.css" rel="stylesheet">

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
    <script src="files/ie-emulation-modes-warning.htm"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- Custom styles for this template -->
    <link href="files/css/carousel.css" rel="stylesheet">
  </head>
  <body>
    <div class="navbar-wrapper">
      <div class="container">

        <div class="navbar navbar-static-top" role="navigation">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
              <a class="navbar-brand" href="#">LAB 3</a>
            </div>
            <div class="navbar-collapse collapse">
              <ul class="nav navbar-nav">
                <li class="active"><a href="index.php">Home</a></li>
                <li><a href="about.php">About</a></li>
                <li><a href="work.php">Work</a></li>  
                <li><a href="artists.php">Artists</a></li>                
              </ul>
            </div>
          </div>
        </div>

      </div>
    </div>

    <!-- Carousel
    ================================================== -->
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
      <!-- Indicators -->
      <ol class="carousel-indicators">
        
        <?php
          $linecount = 0;
          $paintings = fopen("paintings.txt", "r");
          
          while(!feof($paintings) && $linecount < 5){
            $line = fgets($paintings);
            if($linecount == 0) {
              echo "<li data-target='#myCarousel' data-slide-to='0' class='active'></li>";
            }
            else {
              echo "<li class='' data-target='#myCarousel' data-slide-to='". $linecount ."'></li>";
            }
            $linecount += 1;
          }
          
          fclose($paintings);

        ?>
      </ol>
     <div class="carousel-inner">

      <?php
        $paintings = fopen("paintings.txt", "r");
        
        while (!feof($paintings) && $linecount > 0) {
          $line = explode("~", fgets($paintings));
          $linecount -= 1;
          
          if ($line[3] == "01330") {
            echo "<div class='item active'>";
          } else {
            echo "<div class='item'>";
          }
          
          echo "<img src='files/". $line[3] .".jpg' alt='". $line[4] ."' title='". $line[4] ."' rel='#PaintingThumb'>
               <div class='container'>
                  <div class='carousel-caption'>
                  <h1>". $line[4] ."</h1>
                   <p>". $line[6] ."</p>
                   <p><a class='btn btn-lg btn-primary' href='". $line[12] ."' role='button'>Learn more</a></p>
                  </div>
                </div>
              </div>";
        }
        
      ?>
      
    </div>
      <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
      <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    </div>
    

    <div class="container marketing">

      <!-- Three columns of text below the carousel -->
      <?php 

         $count = 0;
         
          while(!feof($paintings) && $count < 7) {
              
              echo "<div class='row'>";
              
              while($count % 4 != 3 && !feof($paintings)) {
                $line = explode("~", fgets($paintings));   
                $count += 1;
                
                echo "<div class='col-lg-4'>
                        <img class='img-circle' src='files/". $line[3] .".jpg' alt='". $line[4] ."' title='". $line[4] ."' style='width:100px; height:100px;'>
                        <h2>". $line[4] ."</h2><p class='text-justify'>". explode(".", $line[5])[0] .".</p>
                        <p><a class='btn btn-default' href='". $line[12] ."' role='button'>View details »</a></p>
                      </div>";
              }
              
              $count += 1;
              echo "</div>";
          }
        fclose($paintings);
      ?>
    </div><!-- /.container -->

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="files/js/jquery.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="files/js/bootstrap.js"></script>
    
  </body>
</html>