<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="us-ansi">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <title>Lab3 SE3316A</title>
        
        <link href='https://fonts.googleapis.com/css?family=Cookie' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Cuprum' rel='stylesheet' type='text/css'>
        <link href="files/css/css.css" rel="stylesheet" type="text/css">      
    
        <!-- Bootstrap core CSS -->
        <link href="files/css/bootstrap.css" rel="stylesheet">
    
        <!-- Custom styles for this template -->
        <link href="files/css/lab3.css" rel="stylesheet">
    
        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
          <script src="../../assets/js/html5shiv.js"></script>
          <script src="../../assets/js/respond.min.js"></script>
        <![endif]-->
   </head>

   <body>

        <header>
        
           <div id="topHeaderRow">
              <div class="container">
                 <nav class="navbar navbar-inverse " role="navigation">
                    <div class="navbar-header">
                       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                       </button>
                       <p class="navbar-text">Welcome to <strong>Art Store</strong>, <a href="#" class="navbar-link">Login</a> or <a href="#" class="navbar-link">Create new account</a></p>
                    </div>
        
                    <div class="collapse navbar-collapse navbar-ex1-collapse pull-right">
                       <ul class="nav navbar-nav">
                          <li><a href="#"><span class="glyphicon glyphicon-user"></span> My Account</a></li>
                          <li><a href="#"><span class="glyphicon glyphicon-gift"></span> Wish List</a></li>
                          <li><a href="#"><span class="glyphicon glyphicon-shopping-cart"></span> Shopping Cart</a></li>
                          <li><a href="#"><span class="glyphicon glyphicon-arrow-right"></span> Checkout</a></li>                  
                       </ul>
                    </div>  <!-- end .collpase --> 
                 </nav>  <!-- end .navbar --> 
              </div>  <!-- end .container --> 
           </div>  <!-- end #topHeaderRow --> 
           
           <div id="logoRow">
              <div class="container">
                 <div class="row">
                    <div class="col-md-8">
                        <h1>Art Store</h1> 
                    </div>
                    
                    <div class="col-md-4">
                       <form class="form-inline" role="search">
                          <div class="input-group">
                             <label class="sr-only" for="search">Search</label>
                             <input class="form-control" placeholder="Search" name="search" type="text">
                             <span class="input-group-btn">
                             <button class="btn btn-default" type="submit"><span class="glyphicon glyphicon-search"></span></button>
                             </span>
                          </div>
                       </form> 
                    </div>   <!-- end .navbar --> 
                 </div>   <!-- end .row -->        
              </div>  <!-- end .container --> 
           </div>  <!-- end #logoRow --> 
           
           <div id="mainNavigationRow">
              <div class="container">
        
                 <nav class="navbar navbar-default" role="navigation">
                    <div class="navbar-header">
                       <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                          <span class="sr-only">Toggle navigation</span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                          <span class="icon-bar"></span>
                       </button>
                    </div>
        
                    <div class="collapse navbar-collapse navbar-ex1-collapse">
                     <ul class="nav navbar-nav">
                       <li><a href="index.php">Home</a></li>
                       <li class="active"><a href="about.php">About Us</a></li>
                       <li><a href="work.php">Art Works</a></li>
                       <li><a href="artists.php">Artists</a></li>
                       <li class="dropdown">
                         <a href="#" class="dropdown-toggle" data-toggle="dropdown">Specials <b class="caret"></b></a>
                         <ul class="dropdown-menu">
                           <li><a href="#">Special 1</a></li>
                           <li><a href="#">Special 2</a></li>                   
                         </ul>
                       </li>
                     </ul>              
                    </div>
                 </nav>  <!-- end .navbar --> 
              </div>  <!-- end container -->
           </div>  <!-- end mainNavigationRow -->
           
        </header>
        
        <div class="container">
              <!-- Main component for a primary marketing message or call to action -->
              <div class="jumbotron">
                <h2>About Us</h2>
                <p>This assignment was created by: <br> <br>
                <em>Hassan Hawilo (hhawilo@uwo.ca)</em> <br>
                <em>Manar Jammal (mjammal@uwo.ca)</em> </p> <br>
                <p>It was created for SE 3316A at Western University.</p>
                <p>
                  <a class="btn btn-lg btn-primary" href="http://owl.uwo.ca/" role="button">Learn more</a>
                </p>
              </div>
        
        </div>  <!-- end container -->
        
        
        
        
            <!-- Bootstrap core JavaScript
            ================================================== -->
            <!-- Placed at the end of the document so the pages load faster -->
            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="files/js/jquery.js"></script>
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="files/js/bootstrap.js"></script>
          
          

    </body>
</html>